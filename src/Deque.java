import java.util.ListIterator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item>{

    private Deque.DLinkedList<Item> list;

    public Deque(){
        list = new Deque.DLinkedList<Item>();
    }

    public boolean isEmpty(){
        return list.length() == 0;
    }

    public int size(){
        return list.length();
    }

    public void addFirst(Item item){

        if (item == null)
            throw new NullPointerException("Cannot add null to the deque.");

        list.prepend(item);

    }

    public void addLast(Item item){

        if (item == null)
            throw new NullPointerException("Cannot add null to the deque.");

        list.append(item);

    }

    public Item removeFirst(){

        if (isEmpty())
            throw new NoSuchElementException("Cannot remove from an empty " +
                    "deque.");

        return list.removeFirst();

    }

    public Item removeLast(){

        if (isEmpty())
            throw new NoSuchElementException("Cannot remove from an empty " +
                    "deque.");

        return list.removeLast();

    }

    public Iterator<Item> iterator(){
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item>{

        Iterator<Item> iterator = list.iterator();

        public boolean hasNext(){
            return iterator.hasNext();
        }

        public Item next(){

            if (!hasNext())
                throw new NoSuchElementException("No elements exist after " +
                        "end of the deque.");

            return iterator.next();

        }

        public void remove(){
            throw new UnsupportedOperationException("The remove operation is" +
                    " unsupported.");
        }

    }

    public static class DLinkedList<Item> implements Iterable<Item>{

        private DNode first;
        private DNode last;
        private int length;

        /**
         * A node that supports links to preceding nodes and succeeding nodes,
         * in addition to storing data.
         */
        private class DNode{

            public Item data;
            public DNode prev;
            public DNode next;

        }

        public DLinkedList(){

            first = new DNode();
            last = new DNode();

            first.next = last;
            last.prev = first;

        }

        public int length(){
            return length;
        }

        public boolean isEmpty(){
            return length == 0;
        }

        public void prepend(Item item){

            if (item == null)
                throw new NullPointerException("Cannot add null to the list.");

            DNode newNode = new DNode();

            newNode.data = item;
            insertAfter(first, newNode);

        }

        public void append(Item item){

            if (item == null)
                throw new NullPointerException("Cannot add null to the list.");

            DNode newNode = new DNode();

            newNode.data = item;
            insertBefore(last, newNode);

        }

        private void insertBefore(DNode node, DNode newNode){

            if (node == null || newNode == null)
                throw new NullPointerException("Neither argument can be null.");

            if (node == first)
                throw new IllegalArgumentException("Cannot insert before the" +
                        " beginning of the list");

            insertAfter(node.prev, newNode);

        }

        private void insertAfter(DNode node, DNode newNode){

            if (node == null || newNode == null)
                throw new NullPointerException("Neither argument can be null.");

            if (node == last)
                throw new IllegalArgumentException("Cannot insert after the " +
                        "end of the list.");

            DNode a = node;
            DNode b = newNode;
            DNode c = node.next;

            b.next = a.next;
            b.prev = c.prev;
            a.next = b;
            c.prev = b;
            length++;

        }

        public Iterator<Item> iterator(){
            return new DLinkedListIterator();
        }

        public void insertAt(Item item, int index){

            if (index < 0 || index >= length)
                throw new IndexOutOfBoundsException("Index: " + index +
                        " is out of bounds.");

            if (item == null)
                throw new NullPointerException("Cannot add null to the list.");

            if (index == 0){
                prepend(item);
                return;
            }
            else if (index == length-1){
                append(item);
                return;
            }
            else{
                DLinkedListIterator iterator = new DLinkedListIterator();

                while (iterator.hasNext() && iterator.nextIndex()-1 != index)
                    iterator.next();

                iterator.add(item);
            }

        }

        public Item getAt(int index){

            if (index < 0 || index >= length)
                throw new IndexOutOfBoundsException("Index: " + index +
                        " is out of bounds.");

            if (index == 0){
                return first.next.data;
            }
            else if (index == length-1){
                return last.prev.data;
            }
            else{
                DLinkedListIterator iterator = new DLinkedListIterator();

                Item item = null;

                while (iterator.hasNext() && iterator.nextIndex()-1 != index)
                    item = iterator.next();

                return item;
            }

        }

        public void setAt(Item item, int index){

            if (index < 0 || index >= length)
                throw new IndexOutOfBoundsException("Index: " + index +
                        " is out of bounds.");

            if (item == null)
                throw new NullPointerException("Cannot set element as null.");

            if (index == 0){
                first.next.data = item;
                return;
            }
            else if (index == length-1){
                last.prev.data = item;
                return;
            }
            else{
                DLinkedListIterator iterator = new DLinkedListIterator();

                while (iterator.hasNext() && iterator.nextIndex()-1 != index)
                    iterator.next();

                iterator.set(item);
            }

        }

        private Item remove(DNode node){

            if (node == null)
                throw new NullPointerException("An argument cannot be null.");

            if (node == first || node == last)
                throw new IllegalArgumentException("Cannot delete sentinel " +
                        "node.");

            DNode a = node.prev;
            DNode c = node.next;

            a.next = c;
            c.prev = a;
            --length;

            return node.data;

        }

        public Item removeFirst(){

            if (isEmpty())
                throw new NoSuchElementException("Cannot remove from an empty" +
                        " list.");

            Item item = remove(first.next);

            return item;

        }

        public Item removeLast(){

            if (isEmpty())
                throw new NoSuchElementException("Cannot remove from an empty" +
                        " list.");

            Item item = remove(last.prev);

            return item;

        }

        public Item removeAt(int index){

            if (index < 0 || index >= length)
                throw new IndexOutOfBoundsException("Index: " + index +
                        " is out of bounds.");

            if (index == 0){
                Item item = removeFirst();
                return item;
            }
            else if (index == length-1){
                Item item = removeLast();
                return item;
            }
            else{
                DLinkedListIterator iterator = new DLinkedListIterator();

                Item item = null;

                while (iterator.hasNext() && iterator.nextIndex()-1 != index)
                    item = iterator.next();

                iterator.remove();

                return item;
            }

        }

        private class DLinkedListIterator implements ListIterator<Item>{

            private DNode current = first.next;
            private DNode lastAccessed = null;
            private int index = 0;

            public boolean hasNext(){
                return index < length;
            }

            public boolean hasPrevious(){
                return index > 0;
            }

            public Item next(){

                if (!hasNext())
                    throw new NoSuchElementException("No next element exists.");

                lastAccessed = current;
                current = current.next;
                index++;

                return lastAccessed.data;
            }

            public Item previous(){

                if (!hasPrevious())
                    throw new NoSuchElementException("No previous element " +
                            "exists.");

                current = current.prev;
                lastAccessed = current.prev;
                index--;

                return lastAccessed.data;
            }

            public int nextIndex(){
                return index;
            }

            public int previousIndex(){
                return index - 1;
            }

            public void remove(){

                if (lastAccessed == null)
                    throw new IllegalStateException();

                DNode a = lastAccessed.prev;
                DNode c = lastAccessed.next;

                a.next = c;
                c.prev = a;
                --length;

                //Occurs after a call to previous()
                if (current == lastAccessed)
                    current = c;
                else --index;

                lastAccessed = null;

            }

            public void set(Item item){

                //when called after add or remove
                if (lastAccessed == null)
                    throw new IllegalStateException();

                lastAccessed.data = item;

            }

            public void add(Item item){

                DNode a = current.prev;
                DNode b = new DNode();
                b.data = item;
                DNode c = current;

                b.prev = a;
                b.next = c;
                a.next = b;
                c.prev = b;
                ++length;
                ++index;
                //So that remove can't be called after add
                lastAccessed = null;

            }
        }
    }//DLinkedList
}//Deque
